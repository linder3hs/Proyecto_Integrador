//
//  RoundButton.swift
//  RecognizediOS
//
//  Created by Linder on 4/19/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable

class RoundButton: UIButton {
    @IBInspectable var roundButton:Bool = false {
        didSet {
            if roundButton {
                layer.cornerRadius = frame.height / 2
            }
        }
    }
    override func prepareForInterfaceBuilder() {
        if roundButton {
            layer.cornerRadius = frame.height / 2
        }
    }
}
