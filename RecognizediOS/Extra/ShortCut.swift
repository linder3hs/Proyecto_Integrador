//
//  ShortCut.swift
//  RecognizediOS
//
//  Created by Linder Hassinger on 9/19/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func makeAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }
    
    func btnCircle(btn:UIButton) {
        btn.layer.shadowColor = UIColor.black.cgColor
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        btn.layer.masksToBounds = false
        btn.layer.shadowRadius = 1.0
        btn.layer.shadowOpacity = 0.5
        btn.layer.cornerRadius = btn.frame.width / 2
    }
    
    func formatDate(fecha:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMMM, dd"
        
        if let date = dateFormatterGet.date(from: fecha){
            return(dateFormatterPrint.string(from: date))
        }
        else {
            return(fecha)
        }
    }
    
    func subString(text:String, start:Int) -> String {
        let str = text
        let indexText = str.index(str.startIndex, offsetBy: start)
        let result = str[..<indexText]
        return String(result)
    }

}


