//
//  RegisterController.swift
//  RecognizediOS
//
//  Created by Linder on 4/1/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
//import TextFieldEffects

class RegisterController: UIViewController {
    @IBOutlet weak var usernameRegister: UITextField!
    @IBOutlet weak var passwordRegister: UITextField!
    @IBOutlet weak var nameRegister: UITextField!
    @IBOutlet weak var lastnameRegister: UITextField!

    var ref: DatabaseReference!
    
    @IBAction func register(_ sender: Any) {
        if usernameRegister.text == "" || passwordRegister.text == "" || nameRegister.text == "" || lastnameRegister.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Rellene los campos", preferredStyle: .alert)
            let actionAlert = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alertController.addAction(actionAlert)
            self.present(alertController, animated: true, completion: nil)
        } else {
            let alertWait = UIAlertController(title: "Validando", message: "Espere...", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50 ))
            spinner.hidesWhenStopped = true
            spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            spinner.startAnimating()
            alertWait.view.addSubview(spinner)
            self.present(alertWait, animated: true, completion: nil)
            Auth.auth().createUser(withEmail: self.usernameRegister.text!, password: self.passwordRegister.text!) { (user, error) in
                if error == nil {
                    self.ref = Database.database().reference()
                    self.ref.child("users").child((user?.uid)!).setValue(["username": self.usernameRegister.text, "name":
                    self.nameRegister.text, "last_name": self.lastnameRegister.text])
                    print("Usuario Creado")
                    alertWait.dismiss(animated: true, completion: nil)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
                    self.present(vc!, animated: true, completion: nil)
                } else {
                    alertWait.dismiss(animated: true, completion: nil)
                    let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    print("Usuario no creado")
                }
            }
        }
    }
    
    
    @IBAction func back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordRegister.isSecureTextEntry = true
        hideKeyBoardWhenTap()
    }
    
    
    
}

extension UIViewController {
    
    func ref() -> DatabaseReference {
        return Database.database().reference()
    }
    func hideKeyBoardWhenTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(RegisterController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
