//
//  PerfilController.swift
//  RecognizediOS
//
//  Created by Linder on 4/1/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseAuth
import SVProgressHUD

class PerfilController: UIViewController {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var logoutOulet: RoundButton!
    @IBOutlet weak var perfilImage: UIImageView!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var lastname: UILabel!
    
    @IBAction func logout(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "¿Seguro que desea cerrar sesión?", preferredStyle: .alert)
        let alertConfirm = UIAlertAction(title: "Si", style: .default) { (_) in
            do {
                try Auth.auth().signOut()
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "First")
                self.present(vc!, animated: true, completion: nil)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        let alertNo = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(alertConfirm)
        alert.addAction(alertNo)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backPress(_ sender: Any) {
        print("Hola si funciona")
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUser()
        perfilImage.layer.borderWidth = 3.0
        perfilImage.layer.masksToBounds = false
        perfilImage.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        perfilImage.layer.cornerRadius = perfilImage.frame.height/2
        perfilImage.clipsToBounds = true
        logoutOulet.layer.borderWidth = 1.0
        logoutOulet.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        viewName.layer.borderWidth = 1.5
        viewName.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        viewLastName.layer.borderWidth = 1.5
        viewLastName.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        let nameUser = Auth.auth().currentUser?.email
        if (nameUser?.isEmpty)! {
            name.text = "Usuario"
        } else {
            name.text = nameUser
        }
        
    }
    
    func loadUser() {
        Database.database().reference().child("users")
            .child((Auth.auth().currentUser?.uid)!).observeSingleEvent(of: .value) { (snashot) in
                let user = User(snapshot: snashot)
                self.username.text = user?.name
                self.lastname.text = user?.lastname
        }
    }
}
