//
//  DatialAulaViewController.swift
//  RecognizediOS
//
//  Created by Linder on 5/7/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class DetialAulaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var nameAula: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var imagePicker = UIImagePickerController()
    var getName = String()
    var chart = [Charts]()
    var refresh: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        nameAula.text = getName
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "Recargando")
        refresh.addTarget(self, action: #selector(DetialAulaViewController.loadChart), for: UIControlEvents.valueChanged)
        tableView.addSubview(refresh)
    }
    
    @objc func loadChart() {
        print("Refresh")
        Database.database().reference().child("users").child((Auth.auth()
            .currentUser?.uid)!).child("chart").observeSingleEvent(of: .value) { (snapshot) in
            let charts = snapshot.children.compactMap {Charts(snapshot: $0 as! DataSnapshot)}
            self.chart.removeAll()
            for chart in charts {
                if chart.curso == self.getName {
                    self.chart.append(chart)
                }
            }
            self.tableView.reloadData()
            self.refresh.endRefreshing()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadChart()
    }
 
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chartCurso(_ sender: Any) {
        performSegue(withIdentifier: "chartCurso", sender: nameAula.text)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetalleChart") as! ChartTableViewCell
        cell.charts = chart[indexPath.row]
        cell.llenarDatos()
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let siguienteVC = segue.destination as! ChartCursoGeneralViewController
        siguienteVC.curso = getName
    }

}

