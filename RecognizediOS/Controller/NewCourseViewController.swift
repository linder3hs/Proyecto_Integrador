//
//  NewCourseViewController.swift
//  RecognizediOS
//
//  Created by Linder on 5/6/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Alamofire
import FirebaseAuth

class NewCourseViewController: UIViewController, UITableViewDelegate,  UITableViewDataSource {
    
    var salones = [Aulas]()
    var refresh: UIRefreshControl!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Connectivity.isConnectedToInternet() {
            tableView.delegate = self
            tableView.dataSource = self
            refresh = UIRefreshControl()
            refresh.attributedTitle = NSAttributedString(string: "Recargando")
            refresh.addTarget(self, action: #selector(NewCourseViewController.loadSalones), for: UIControlEvents.valueChanged)
            tableView.addSubview(refresh)
        } else {
            let alert = UIAlertController(title: "Error", message: "No hay conexión a internet", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
        
    }
    
    @objc func loadSalones() {
        Database.database().reference().child("aulas").observeSingleEvent(of: .value) { (snapshot) in
            let aulas = snapshot.children.compactMap {Aulas(snapshot: $0 as! DataSnapshot)}
            self.salones.removeAll()
            for aula in aulas {
                if aula.user_id == Auth.auth().currentUser?.uid {
                    print(aula.nombre_curso)
                    self.salones.append(aula)
                }
            }
            /*if let result = snapshot.children.allObjects as? [DataSnapshot] {
                for child in result {
                    let orderID = child.key
                    print(orderID)
                }
            }*/
            self.tableView.reloadData()
            self.refresh.endRefreshing()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadSalones()
        print("Reload Data")
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return salones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if salones.isEmpty {
            cell.textLabel?.text = "No hay Internet"
            refresh.endRefreshing()
        } else {
            cell.textLabel?.text = salones[indexPath.row].nombre_curso
            cell.imageView?.image = UIImage(named: "ebook")
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let dvc = storyBoard.instantiateViewController(withIdentifier: "detalle") as! DetialAulaViewController
        dvc.getName = salones[indexPath.row].nombre_curso
        self.navigationController?.pushViewController(dvc, animated: true)
    }
  
}
