//
//  AddLibraryViewController.swift
//  RecognizediOS
//
//  Created by Linder on 5/27/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
import SVProgressHUD
import Alamofire

class AddLibraryViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var salones = [Aulas]()
    var dropsalones = UIPickerView()
    @IBOutlet weak var txtsalon: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnSave: RoundButton!
    var emotions = [String]()
    var topEmotion = ""
    var imagenURL = ""
    var imagePicker = UIImagePickerController()
    var emotionArray = [Emotion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSave.layer.borderWidth = 1.0
        btnSave.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        loadSalones()
        hideKeyBoardWhenTap()
        dropsalones.delegate = self
        imagePicker.delegate = self
        txtsalon.inputView = dropsalones
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.tintColor = #colorLiteral(red: 0.3215686275, green: 0.7450980392, blue: 0.5019607843, alpha: 1)
        toolbar.isUserInteractionEnabled = true
        let doneButton = UIBarButtonItem(title: "Hecho", style: .done, target: self, action: #selector(self.dismissKeyboard))
        toolbar.setItems([doneButton], animated: true)
        txtsalon.inputAccessoryView = toolbar
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
        imageView.backgroundColor = UIColor.clear
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @objc func loadSalones() {
        Database.database().reference().child("aulas").observeSingleEvent(of: .value) { (snapshot) in
            let aulas = snapshot.children.compactMap {Aulas(snapshot: $0 as! DataSnapshot)}
            let info = snapshot.key
            print("IDSalon")
            print(info)
            print("Snapshot value")
            let val = snapshot.value as! NSDictionary
            print(val.allKeys)
            self.salones.removeAll()
            let user_id:String = (Auth.auth().currentUser?.uid)!
            for aula in aulas {
                print("Aula")
                print(aula.nombre_curso)
                if aula.user_id == user_id {
                    self.salones.append(aula)
                }
            }
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int { return 1 }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let cont : Int = salones.count
        return cont
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == dropsalones {
            let title = salones[row]
            return title.nombre_curso
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == dropsalones {
            let title = salones[row]
            self.txtsalon.text = title.nombre_curso
        }
    }
    
    @IBAction func openCamera(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let actiontakePhoto = UIAlertAction(title: "Tomar Foto", style: .default) { (action) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let actionChoosePhoto = UIAlertAction(title: "Escoger Foto", style: .default) { (action) in
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let actioncancel = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        actioncancel.setValue(UIColor.red, forKey: "titleTextColor")
        alert.addAction(actiontakePhoto)
        alert.addAction(actionChoosePhoto)
        alert.addAction(actioncancel)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func savePhotoAula(_ sender: Any) {
        if self.txtsalon.text != "" {
            var img = ""
            SVProgressHUD.show(withStatus: "Analizando foto...")
            let user_id = Auth.auth().currentUser?.uid
            let imageFolder = Storage.storage().reference().child("imagenes")
            let imageData = UIImageJPEGRepresentation(imageView.image!, 0.25)
            //Save image in firebase
            imageFolder.child("\(NSUUID().uuidString).jpg").putData(imageData!, metadata: nil, completion: { ( metadata, error ) in
                print("Intentando subir la imagen")
                img = (metadata?.downloadURL()?.absoluteString)!
                self.ref().child("photoaula").childByAutoId().setValue(["aula": self.txtsalon.text!, "user_id": user_id!, "urlimage": metadata?.downloadURL()?.absoluteString])
                // Post Alamofire
                let url = "https://southcentralus.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=emotion"
                let header = ["Content-Type" : "application/json", "Ocp-Apim-Subscription-Key": "37bddc164e27454db27b2c373b23e99f"]
                let params = ["url" : img]
                self.imagenURL = img
                Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).validate().responseJSON(completionHandler: { (response) in
                    switch response.result {
                    case .success:
                        if let personArray = response.result.value as? [Dictionary<String, AnyObject>] {
                            SVProgressHUD.dismiss()
                            self.emotions.removeAll()
                            self.emotionArray.removeAll()
                            for person in personArray {
                                var top = 0
                                var left = 0
                                var height = 0
                                var width = 0
                                if let faceAttributes = person["faceAttributes"] as? Dictionary<String, AnyObject> {
                                    let emotionRectangle = person["faceRectangle"] as? Dictionary<String, AnyObject>
                                    top = emotionRectangle!["top"] as! Int
                                    left = emotionRectangle!["left"] as! Int
                                    height = emotionRectangle!["height"] as! Int
                                    width = emotionRectangle!["width"] as! Int
                                    print("Medidas Azure")
                                    print(top)
                                    print(left)
                                    print(height)
                                    print(width)
                                    if let emotionDictionary = faceAttributes["emotion"] as? Dictionary<String, AnyObject> {
                                        var topValue:Double = 0.0
                                        for emotion in emotionDictionary.keys {
                                            let value = emotionDictionary[emotion] as? Double
                                            if value! > topValue {
                                                topValue = value!
                                                self.topEmotion = emotion
                                                self.emotions.append(self.topEmotion)
                                            }
                                        }
                                    }
                                }
                                let emotion = Emotion(emo: self.topEmotion, img: self.imagenURL, x: top, y: left, height: height, width: width)
                                self.emotionArray.append(emotion)
                            }
                            SVProgressHUD.dismiss()
                            self.performSegue(withIdentifier: "detalleFoto", sender: self.emotionArray)
                        }
                    case .failure(let error):
                        SVProgressHUD.dismiss()
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                    }
                })
                if error != nil {
                    print("Ocurrio un error \(String(describing: error))")
                } else {
                    print("No hay error")
                }
            })
            
        } else {
            let alert = UIAlertController(title: "Ojo", message: "Rellene el aula", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let siguienteVC = segue.destination as! DetalleFotoViewController
        siguienteVC.emotionArray = emotionArray
        siguienteVC.curso = txtsalon.text!
    }
    
}
