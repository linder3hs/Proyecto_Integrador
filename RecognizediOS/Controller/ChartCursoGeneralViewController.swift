//
//  ChartCursoGeneralViewController.swift
//  RecognizediOS
//
//  Created by Linder on 7/3/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import SwiftChart
import Firebase
import FirebaseDatabase
import FirebaseAuth

class ChartCursoGeneralViewController: UIViewController, ChartDelegate {
    
    var curso = String()
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var chartView: Chart!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCursoGenerelChart()
        name.text = curso
        print(curso)
    }

    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadCursoGenerelChart() {
        var a = 0
        var h = 0
        var s = 0
        var c = 0
        var p = 0
        var d = 0
        var n = 0
        var f = 0
        Database.database().reference().child("users")
            .child((Auth.auth().currentUser?.uid)!).child("chart").observeSingleEvent(of: .value) { (snapshot) in
                let charts = snapshot.children.compactMap{Charts(snapshot: $0 as! DataSnapshot)}
                for chart in charts {
                    if chart.curso == self.curso {
                        a = chart.anger + a
                        h = chart.happiness + h
                        s = chart.sadness + s
                        c = chart.contempt + c
                        p = chart.surprise + p
                        d = chart.disgust + d
                        n = chart.neutral + n
                        f = chart.fear + f
                    }
                }
                
                self.chartView.delegate = self
                let anger = Double(a)
                let happiness = Double(h)
                let sadness = Double(s)
                let contempt = Double(c)
                let surprise = Double(p)
                let disgust = Double(d)
                let neutral = Double(n)
                let fear = Double(f)
                let serie = ChartSeries([anger, happiness, sadness, contempt, surprise, disgust, neutral, fear])
                serie.colors = (
                    above: ChartColors.purpleColor(),
                    below: ChartColors.redColor(),
                    zeroLevel: 10
                )
                let labels: [Double] = [0, 1, 2, 3, 4, 5, 6, 7]
                var labelsAsString: Array<String> = ["😡", "😄", "😢", "😧", "😱", "😏", "😐", "😰"]
                
                self.chartView.xLabels = labels
                self.chartView.xLabelsFormatter = { (labelIndex: Int, labelValue: Double) -> String in
                    return labelsAsString[labelIndex]
                }
                self.chartView.add(serie)
        }
    }
    
    func didTouchChart(_ chart: Chart, indexes: [Int?], x: Double, left: CGFloat) {
        print("DidTouch")
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        print("DidFinish")
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        print("DidEnding")
    }
}
