//
//  ViewController.swift
//  RecognizediOS
//
//  Created by Linder on 4/1/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import RevealingSplashView
import SVProgressHUD

class ViewController: UIViewController, UITextFieldDelegate {
    // MARK: - Variables
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var btnLogin: RoundButton!
    
    let revealingSplashView = RevealingSplashView(iconImage: UIImage(named:"logo-f")!, iconInitialSize: CGSize(width: 80, height: 80), backgroundColor: #colorLiteral(red: 0.1298420429, green: 0.1298461258, blue: 0.1298439503, alpha: 1))
    
    // MARK: - Funciones
    @IBAction func login(_ sender: Any) {
        if username.text == "" || password.text == "" {
            
            let alertController = UIAlertController(title: nil, message: "Rellene todos los campos", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        } else {
            SVProgressHUD.show(withStatus: "Cargando...")
            Auth.auth().signIn(withEmail: self.username.text!, password: self.password.text!) { ( user, error ) in
                if error == nil {
                    print("Login success")
                    SVProgressHUD.dismiss()
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
                    self.present(vc!, animated: true, completion: nil)
                } else {
                    SVProgressHUD.dismiss()
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    // MARK: - LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        let myColor = #colorLiteral(red: 0.3215686275, green: 0.7450980392, blue: 0.5019607843, alpha: 1)
        username.delegate = self
        username.tag = 0
        password.delegate = self
        password.tag = 1
        password.isSecureTextEntry = true
        btnLogin.layer.borderColor = myColor.cgColor
        btnLogin.layer.borderWidth = 1.5
        username.layer.sublayerTransform = CATransform3DMakeTranslation(5.0, 5.0, 5.0)
        password.layer.sublayerTransform = CATransform3DMakeTranslation(5.0, 5.0, 5.0)
        
        self.view.addSubview(self.revealingSplashView)
        self.revealingSplashView.animationType = SplashAnimationType.twitter
        self.revealingSplashView.startAnimation()
        if Auth.auth().currentUser != nil {
            print("User logged")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
            self.present(vc!, animated: true, completion: nil)
        } else {
            print("User no login")
        }
        hideKeyBoardWhenTap()
    
    }

}

