//
//  HomeController.swift
//  RecognizediOS
//
//  Created by Linder on 4/1/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import UIKit
import CoreImage
import Vision
import SwiftChart
import Firebase
import FirebaseAuth
import FirebaseDatabase

class HomeController:UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, ChartDelegate {
    
    @IBOutlet weak var chartView: Chart!
    @IBOutlet weak var legenView: UIView!
    var chart: Charts!
    var molesto = [Double]()
    var happy = [Double]()
    var contempt = [Double]()
    var disgut = [Double]()
    var fear = [Double]()
    var neutral = [Double]()
    var surprise = [Double]()
    var sad = [Double]()
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeController.dismissKeyBoard))
        view.addGestureRecognizer(tap)
        loadChartGeneral()
        legenView.layer.cornerRadius = 8.0
    }
    
    func loadChartGeneral() {
        var a = 0
        var h = 0
        var s = 0
        var c = 0
        var p = 0
        var d = 0
        var n = 0
        var f = 0
        Database.database().reference().child("users")
            .child((Auth.auth().currentUser?.uid)!).child("chart")
            .observeSingleEvent(of: .value) { (snapshot) in
            let chartsGeneral = snapshot.children.compactMap {
                ChartGeneral(snapshot: $0 as! DataSnapshot)
            }
                
            for generalChart in chartsGeneral {
                a = generalChart.anger + a
                h = generalChart.happiness + h
                s = generalChart.sadness + s
                c = generalChart.contempt + c
                p = generalChart.surprise + p
                d = generalChart.disgust + d
                n = generalChart.neutral + n
                f = generalChart.fear + f
            }
            self.chartView.delegate = self
            let anger = Double(a)
            let happiness = Double(h)
            let sadness = Double(s)
            let contempt = Double(c)
            let surprise = Double(p)
            let disgust = Double(d)
            let neutral = Double(n)
            let fear = Double(f)
            let serie = ChartSeries([anger, happiness, sadness, contempt, surprise, disgust, neutral, fear])
            serie.colors = (
                above: ChartColors.darkGreenColor(),
                below: ChartColors.redColor(),
                zeroLevel: 10
            )
            let labels: [Double] = [0, 1, 2, 3, 4, 5, 6, 7]
            var labelsAsString: Array<String> = ["😡", "😄", "😢", "😧", "😱", "😏", "😐", "😰"]
            
            self.chartView.xLabels = labels
            self.chartView.xLabelsFormatter = { (labelIndex: Int, labelValue: Double) -> String in
                return labelsAsString[labelIndex]
            }
            self.chartView.add(serie)
    }
        
    }
    
    func didTouchChart(_ chart: Chart, indexes: [Int?], x: Double, left: CGFloat) {
        print("DidTouch")
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        print("DidFinish")
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        print("DidEnding")
    }
    
    @objc func dismissKeyBoard() {
        view.endEditing(true)
    }
}
