//
//  RegisterCourseViewController.swift
//  RecognizediOS
//
//  Created by Linder on 5/6/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import FirebaseDatabase
import FirebaseAuth

class RegisterCourseViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var txtcarrera: UITextField!
    var dropCarrera = UIPickerView()
    var dropCiclos = UIPickerView()
    var dropAnios = UIPickerView()
    var dropHoraI = UIDatePicker()
    var dropHoraF = UIDatePicker()
    
    @IBOutlet weak var add: RoundButton!
    @IBOutlet weak var nameRoom: UITextField!
    @IBOutlet weak var ciclo: UITextField!
    @IBOutlet weak var aula: UITextField!
    @IBOutlet weak var horaI: UITextField!
    @IBOutlet weak var horaF: UITextField!
    @IBOutlet weak var anio: UITextField!
    
    // Mark Arrays
    var listCarrera = ["Administración de Redes y Comunicaciones" , "Aviónica y Mecánica Aeronáutica" , "Diseño y Desarrollo de Simuladores y Videojuegos" , "Diseño y Desarrollo de Software", "Diseño Industrial", "Electrónica y Automatización Industrial", "Electricidad Industrial", "Gestión y Mantenimiento de Maquinaria Industrial", "Gestión y Mantenimiento de Maquinaria Pesada", "Mecatrónica Industrial", "Operaciones Mineras", "Producción y Gestión Industrial", "Procesos Químicos y Metalúrgicos"]
    var listCiclos = ["1", "2", "3", "4", "5", "6"]
    var listAnios = ["2018 - 1", "2018 - 2"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropCarrera.delegate = self
        dropCiclos.delegate = self
        dropAnios.delegate = self
        anio.inputView = dropAnios
        txtcarrera.inputView = dropCarrera
        ciclo.inputView = dropCiclos
        nameRoom.delegate = self
        nameRoom.tag = 0
        txtcarrera.delegate = self
        txtcarrera.tag = 1
        aula.delegate = self
        aula.tag = 2
        ciclo.delegate = self
        ciclo.tag = 3
        horaI.delegate = self
        horaI.tag = 4
        horaF.delegate = self
        horaF.tag = 5
        anio.delegate = self
        anio.tag = 6
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.isUserInteractionEnabled = true 
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.dismissKeyboard))
        toolbar.setItems([doneButton], animated: true)
        txtcarrera.inputAccessoryView = toolbar
        ciclo.inputAccessoryView = toolbar
        horaI.inputAccessoryView = toolbar
        anio.inputAccessoryView = toolbar
        horaI.inputAccessoryView = toolbar
        horaF.inputAccessoryView = toolbar
        dropHoraI.addTarget(self, action: #selector(RegisterCourseViewController.datePickerValueChangedI(_:)), for: .valueChanged)
        dropHoraF.addTarget(self, action: #selector(RegisterCourseViewController.datePickerValueChangedF(_:)), for: .valueChanged)

        dropHoraI.datePickerMode = UIDatePickerMode.time
        horaI.inputView = dropHoraI
        dropHoraF.datePickerMode = UIDatePickerMode.time
        horaF.inputView = dropHoraF
        hideKeyBoardWhenTap()
        let myColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        toolbar.tintColor = myColor
        add.layer.borderColor = myColor.cgColor
        add.layer.borderWidth = 1.5
        txtcarrera.text = listCarrera[0]
    }
    
    @objc func datePickerValueChangedI(_ sender: UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let selectedDate: String = dateFormatter.string(from: dropHoraI.date)
        horaI.text = selectedDate
        print("Selected value \(selectedDate)")
    }
    
    @objc func datePickerValueChangedF(_ sender: UIDatePicker){
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        let selectedDate: String = dateFormatter.string(from: dropHoraF.date)
        horaF.text = selectedDate
        print("Selected value \(selectedDate)")
    }

    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == dropCarrera {
            return listCarrera.count
        } else if pickerView == dropCiclos {
            return listCiclos.count
        } else if pickerView == dropAnios {
            return listAnios.count
        }
        return 1
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == dropCarrera {
            let title = listCarrera[row]
            return title
        } else if pickerView == dropCiclos {
            let title = listCiclos[row]
            return title
        } else if pickerView == dropAnios {
            let title = listAnios[row]
            return title
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == dropCarrera {
            self.txtcarrera.text = self.listCarrera[row]
        } else if pickerView == dropCiclos {
            self.ciclo.text = self.listCiclos[row]
        } else if pickerView == dropAnios {
            self.anio.text = self.listAnios[row]
        }
    }
    
    @IBAction func addAul(_ sender: Any) {
        if nameRoom.text! == "" || txtcarrera.text! == "" || ciclo.text! == "" || horaI.text! == "" || horaF.text! == "" || aula.text! == "" || anio.text! == "" {
            let alert = UIAlertController(title: "Ojo", message: "Rellene todos los campos", preferredStyle: .alert)
            let actionAlert = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(actionAlert)
            present(alert, animated: true, completion: nil)
        } else {
            let user_id = Auth.auth().currentUser?.uid
            self.ref().child("aulas").childByAutoId().setValue(["nombre_curso" : self.nameRoom.text, "carrera" : self.txtcarrera.text, "ciclo": self.ciclo.text, "user_id": user_id, "aula": self.aula.text, "horaI": self.horaI.text, "horaF": self.horaF.text, "anio": self.anio.text])
            navigationController?.popViewController(animated: true)
            print("Todo bien")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    
    
    
}
