//
//  DetalleFotoViewController.swift
//  RecognizediOS
//
//  Created by Linder on 6/11/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase
import FirebaseDatabase
import FirebaseAuth

class DetalleFotoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var emotionArray = [Emotion]()
    var curso:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 40, right: 0)
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emotionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FotoCropTableViewCell") as! FotoCropTableViewCell
        cell.emo = emotionArray[indexPath.row]
        cell.llenarDatos()
        return cell
    }
    
    @IBAction func saveDatos(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: "¿Seguro que desea guardar los datos?", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Si", style: .default) { (_) in
            self.alertYes()
        }
        let alertActionNo = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        alert.addAction(alertActionNo)
        present(alert, animated: true, completion: nil)
    }
    
    func alertYes() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm a"
        let result = formatter.string(from: date)
        var emocion = ""
        var anger:Int = 0
        var contempt:Int = 0
        var disgust:Int = 0
        var fear:Int = 0
        var happiness:Int = 0
        var neutral:Int = 0
        var sadness:Int = 0
        var surprise:Int = 0
        for emotion in emotionArray {
            emocion = emotion.emotion
            if emocion == "anger" {
                anger = anger + 1
            } else if emocion == "contempt" {
                contempt = contempt + 1
            } else if emocion == "disgust" {
                disgust = disgust + 1
            } else if emocion == "fear" {
                fear = fear + 1
            } else if emocion == "happiness" {
                happiness = happiness + 1
            } else if emocion == "neutral" {
                neutral = neutral + 1
            } else if emocion == "sadness" {
                sadness = sadness + 1
            } else if emocion == "surprise" {
                surprise = surprise + 1
            }
        }
        self.ref().child("users").child((Auth.auth().currentUser?.uid)!)
            .child("chart").childByAutoId().setValue(["fecha": result, "curso": self.curso, "anger": anger, "contempt": contempt, "disgust": disgust, "fear": fear, "happiness": happiness, "neutral": neutral, "sadness": sadness, "surprise": surprise])
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
        self.present(vc!, animated: true, completion: nil)
    }
    
}
