//
//  EditProfileViewController.swift
//  RecognizediOS
//
//  Created by Linder on 7/3/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SVProgressHUD

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var lastnameUser: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyBoardWhenTap()
    }
    
    @IBAction func backButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func EditButton(_ sender: Any) {
        if nameUser.text != "" && lastnameUser.text != "" {
            let alert = UIAlertController(title: nil, message: "¿Seguro de editar sus datos?", preferredStyle: .alert)
            let alertConfirm = UIAlertAction(title: "Si", style: .default) { (_) in
                self.updateProfile()
            }
            let alertNo = UIAlertAction(title: "No", style: .cancel, handler: nil)
            alert.addAction(alertConfirm)
            alert.addAction(alertNo)
            present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: nil, message: "Rellene todos los datos", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(alertAction)
            present(alert, animated: true, completion: nil)
        }
        
    }

    func updateProfile() {
        let ref = Database.database().reference().child("users").child((Auth.auth().currentUser?.uid)!)
        ref.updateChildValues([
            "name": nameUser.text!,
            "last_name": lastnameUser.text!
            ])
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home")
        self.present(vc!, animated: true, completion: nil)
    }
}
