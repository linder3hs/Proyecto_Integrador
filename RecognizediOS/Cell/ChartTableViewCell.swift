//
//  ChartTableViewCell.swift
//  RecognizediOS
//
//  Created by Linder on 7/1/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit
import SwiftChart

class ChartTableViewCell: UITableViewCell, ChartDelegate {
        
    @IBOutlet weak var fecha: UILabel!
    @IBOutlet weak var chartView: Chart!
    
    var charts: Charts?
    override func awakeFromNib() {
        super.awakeFromNib()
        chartView.delegate = self
        // Initialization code
    }
    
    func llenarDatos() {
        if charts != nil {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd-MM-yyyy HH:mm"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "MMM dd,yyyy HH:mm a"
            let fec = ((charts?.fecha)!.prefix(17))
            print(String(fec))
            
            if let date = dateFormatterGet.date(from: String(fec)){
                
                print(dateFormatterPrint.string(from: date))
                fecha.text = dateFormatterPrint.string(from: date)
            }
            else {
                fecha.text = "Hay algún problema con la fecha"
                print("There was an error decoding the string")
            }
            let anger = Double((charts?.anger)!)
            let contempt = Double((charts?.contempt)!)
            let disgust = Double((charts?.disgust)!)
            let fear = Double((charts?.fear)!)
            let happiness = Double((charts?.happiness)!)
            let neutral = Double((charts?.neutral)!)
            let surprise = Double((charts?.surprise)!)
            let sadness = Double((charts?.sadness)!)
            let serie = ChartSeries([anger, contempt, disgust, fear, happiness, sadness, neutral, surprise])
            serie.area = true
            let labels: [Double] = [0, 1, 2, 3, 4, 5, 6, 7]
            var labelsAsString: Array<String> = ["😡", "😏", "😧", "😰", "😄", "😐", "😱", "😢"]
            
            chartView.xLabels = labels
            chartView.xLabelsFormatter = { (labelIndex: Int, labelValue: Double) -> String in
                return labelsAsString[labelIndex]
            }
            
            serie.colors = (
                above: ChartColors.greenColor(),
                below: ChartColors.redColor(),
                zeroLevel: 3
            )
            chartView.add(serie)
        }
    }
 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didTouchChart(_ chart: Chart, indexes: [Int?], x: Double, left: CGFloat) {
        print("DidTouch")
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        print("DidFinish")
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        print("DidEnding")
    }

}
