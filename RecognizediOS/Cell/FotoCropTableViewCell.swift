//
//  FotoCropTableViewCell.swift
//  RecognizediOS
//
//  Created by Linder on 6/15/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import UIKit

class FotoCropTableViewCell: UITableViewCell {
    @IBOutlet weak var imagenCrop: UIImageView!
    @IBOutlet weak var emotion: UILabel!
    @IBOutlet weak var emojiImage: UIImageView!
    
    var emo: Emotion?
    
    @IBOutlet weak var contenedor: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contenedor.layer.cornerRadius = 4
        contenedor.layer.borderWidth = 1.0
        contenedor.layer.borderColor = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
        contenedor.layoutMargins.bottom = 20
    }
    
    func llenarDatos() {
        if emo != nil {
            let emoF:String
            if emo?.emotion == "anger" {
                emoF = "Molesto"
                emojiImage.image = UIImage(named: "enojo")
                
            } else if(emo?.emotion == "contempt") {
                emoF = "Desprecio"
                emojiImage.image = UIImage(named: "desprecio")
                
            } else if(emo?.emotion == "disgust") {
                emoF = "Disgustado"
                emojiImage.image = UIImage(named: "disgustado")
                
            } else if (emo?.emotion == "fear") {
                emoF = "Miedo"
                emojiImage.image = UIImage(named: "miedo")
                
            } else if(emo?.emotion == "happiness") {
                emoF = "Feliz"
                emojiImage.image = UIImage(named: "feliz")
                
            } else if(emo?.emotion == "sadness") {
                emoF = "Triste"
                emojiImage.image = UIImage(named: "triste")
                
            } else if(emo?.emotion == "surprise") {
                emoF = "Sorprendido"
                emojiImage.image = UIImage(named: "sorprendido")
            } else if(emo?.emotion == "neutral") {
                emoF = "Neutral"
                emojiImage.image = UIImage(named: "neutral")
            } else {
                emoF = "Emoción \n no encontrada"
                emojiImage.image = UIImage(named: "thinking")
            }
            emotion.text = emoF
            imagenCrop.sd_setImage(with: URL(string: emo!.img)) { (img, error, _, _) in
                if error == nil {
                    let cropImage = self.emo!.cropImage(image: img!.fixImageOrientation()!)
                    self.imagenCrop.image = cropImage
                } else {
                    print("Error: " + (error?.localizedDescription)!)
                }
            }
        }
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
