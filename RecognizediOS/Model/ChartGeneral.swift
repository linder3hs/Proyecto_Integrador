//
//  ChartGeneral.swift
//  RecognizediOS
//
//  Created by Linder on 7/2/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class ChartGeneral {
    let fecha: String
    let curso: String
    let anger: Int
    let contempt: Int
    let disgust: Int
    let fear: Int
    let happiness: Int
    let sadness: Int
    let neutral: Int
    let surprise: Int
    
    init?(snapshot: DataSnapshot) {
        guard let dic = snapshot.value as? [String:Any],
            let fecha = dic["fecha"] as? String,
            let curso = dic["curso"] as? String,
            let anger = dic["anger"] as? Int,
            let contempt = dic["contempt"] as? Int,
            let disgust = dic["disgust"] as? Int,
            let fear = dic["fear"] as? Int,
            let happiness = dic["happiness"] as? Int,
            let sadness = dic["sadness"] as? Int,
            let neutral = dic["neutral"] as? Int,
            let surprise = dic["surprise"] as? Int
            else {
                return nil
        }
        
        self.fecha = fecha
        self.curso = curso
        self.anger = anger
        self.contempt = contempt
        self.disgust = disgust
        self.fear = fear
        self.happiness = happiness
        self.sadness = sadness
        self.neutral = neutral
        self.surprise = surprise
    }
}
