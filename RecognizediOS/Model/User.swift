//
//  User.swift
//  RecognizediOS
//
//  Created by Linder on 7/3/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import FirebaseDatabase

class User {
    
    let name:String
    let lastname:String
    let username:String
    let tipo:String
    
    init?(snapshot:DataSnapshot) {
        guard let dic = snapshot.value as? [String:Any],
        let name = dic["name"] as? String,
        let lastname = dic["last_name"] as? String,
        let username = dic["username"] as? String,
        let tipo = dic["tipo"] as? String
            else {
                return nil
        }
        
        self.name = name
        self.lastname = lastname
        self.username = username
        self.tipo = tipo
    }
    
    
}
