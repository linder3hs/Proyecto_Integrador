//
//  Aula.swift
//  RecognizediOS
//
//  Created by Linder on 5/6/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Aulas {
    
    let nombre_curso : String
    let carrera : String
    let ciclo : String
    let user_id: String
     
     init?(snapshot : DataSnapshot) {
        guard let dic = snapshot.value as? [String:Any],
        let nombre_curso = dic["nombre_curso"] as? String,
        let carrera = dic["carrera"] as? String,
        let ciclo = dic["ciclo"] as? String,
        let user_id = dic["user_id"] as? String
        else {
            return nil
        }
        
            self.nombre_curso = nombre_curso
            self.carrera = carrera
            self.ciclo = ciclo
            self.user_id = user_id
     }
     
}
