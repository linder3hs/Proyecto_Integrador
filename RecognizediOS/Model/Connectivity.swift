//
//  File.swift
//  RecognizediOS
//
//  Created by Linder on 5/9/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
