//
//  Emtion.swift
//  RecognizediOS
//
//  Created by Linder on 6/22/18.
//  Copyright © 2018 Linder. All rights reserved.
//

import Foundation
import AVKit
import Alamofire
import AlamofireImage

class Emotion {
    
    var emotion = ""
    var img = ""
    var x = 0
    var y = 0
    var height = 0
    var width = 0
    
    init(emo:String, img:String, x:Int, y:Int, height:Int, width:Int) {
        self.emotion = emo
        self.x = x
        self.y = y
        self.img = img
        self.height = height
        self.width = width
    }
    
    func cropImage(image: UIImage) -> UIImage {
        let croppedCGImage = image.cgImage?.cropping(to: CGRect(x: self.y, y: self.x, width: self.width, height: self.height))
        let croppedImage = UIImage(cgImage: croppedCGImage!)
        return croppedImage

    }
}
